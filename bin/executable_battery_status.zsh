# VARIABLE names must be in upper case
# FUNCTION names must be in lower case
# All name use snake_case

# Reminder
# apm -b to get status (3 if charging)
# apm -l to get %
# apm -t to get remaining time

# Phase 1
# TODO: Print the battery level in percentage
# TODO: Print the battery status (Charging, discharging)
# TODO: Notify if the level got below 15%

# Phase 2
# TODO: Replace battery status by icons from nerd font depending of the battery 
# level.
# See: https://www.nerdfonts.com/cheat-sheet
# from f578 to f58a
# TODO: Add sound to notification alert (Move to Dunst TODO?)

BAT_LVL=$(apm -l)

echo ${BAT_LVL}%
