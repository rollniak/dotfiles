set syntax=on
set tabstop=4 softtabstop=4
set shiftwidth=4
set colorcolumn=81
set smarttab
"set relativenumber
"set number
set scrolloff=16

" Vim-plug
call plug#begin('~/.local/share/nvim/plugged')

" GuvBox Coorscheme
Plug 'https://github.com/morhetz/gruvbox'

call plug#end()

" Colorscheme
set termguicolors
set background=dark
au colorscheme gruvbox hi Normal ctermbg=none guibg=none
